# Deutsche Übersetzung für Screenkey
## Info
Url der Software: (https://www.thregr.org/~wavexx/software/screenkey/)

## Installation

1. Das Verzeichnis in das VerZeichnis ../locales kopieren 
2. Setup der Deutschen Sprachdatei mit
``./setup.py init_catalog -l de``
3. Screenkey starten
``./screenkey``
