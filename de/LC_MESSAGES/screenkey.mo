��    �      �              L	  K   M	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     

     
  	   
     )
     0
     6
     =
     B
     I
     O
     W
     `
     w
     }
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
  
   �
     �
     �
     �
     �
          	               $     *     0     4     ;     J     P     U     Z     a     h     p     v     �     �     �     �     �     �     �     �     �     �     �     �     �    �     �                  
   9     D  	   _     i     n     t     z  
   �     �     �  
   �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �                                             #     '     +     /     3     7     ;     ?     C     H  �  M  X   �     I     M     Q     U     Y     ]     a     e     i     m     q     u     y     }     �     �     �     �     �     �     �     �  	   �     �     �     �     �     �          
       $        @     F  
   K  
   V  	   a     k     q     v     z     }     �     �     �     �     �     �     �     �     �     �     �     �     �     �  
   �     �     �     �     �     �     �     �                         #     (     -     6     =  	   A     K     Q     e     m     u     ~     �     �     �     �     �     �     �  
   �     �  d  �     E     L  	   c  !   m  
   �     �     �     �     �     �     �  
   �     �     �  
   �     �     �                                   !     %     )     -     1     6     ;     @     D     H     L     P     T     X     Z     ^     b     f     j     n     r     v     z     ~     �     �     �     �     �     �     �     �   "slop" is required for interactive selection. See <a href="{url}">{url}</a> (*) (+) (-) (.) (/) (0) (1) (2) (3) (4) (5) (6) (7) (8) (9) About Alt+ AltGr+ Always show Shift Background color Backspace mode Baked Bluetooth Bottom Break Bright Caps Center Color Compose Composed Compress repeats after Ctrl+ Del Display Display for Eject Emacs End Esc F1 F10 F11 F12 F2 F3 F4 F5 F6 F7 F8 F9 Fixed Font Font color Full Hide duration Home Hyper+ Ins Keyboard mode Keys Keysyms Large Linux Mac Medium Modifiers mode Mouse Mute Next Normal NumLck Opacity Pause Persistent window PgDn PgUp Position Preferences Prev Print Quit Raw Rec Reset ScrLck Screen Screencast your keys Screenkey failed to initialize. This is usually a sign of an improperly configured input method or desktop keyboard settings. Please see the <a href="{url}">troubleshooting documentation</a> for further diagnosing instructions.

Screenkey cannot recover and will now quit! Search Select window/region Shift+ Show Modifier sequences only Show Mouse Show Whitespace characters Show keys Size Small Super Super+ Text color Time Top Translated Vol WLAN Win Windows off on seconds ← ↑ → ↓ ↹ ⇧+ ⌘+ ⌥+ ⌫ ⏎ ␣ ▶ ⬛                   🛠 🟊 Project-Id-Version: screenkey 1.4
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2021-02-13 01:39+0100
PO-Revision-Date: 2021-02-13 23:52+0100
Last-Translator: Maik Schmalle <maik@masilux.de>
Language: de_DE
Language-Team: German <kde-i18n-de@kde.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.9.0
 "Slop" ist für die interaktive Auswahl erforderlich. Siehe <ahref = "{url}"> {url} </a> (*) (+) (-) (.) (/) (0) (1) (2) (3) (4) (5) (6) (7) (8) (9) Info Alt+ AltGr+ Immer Umschalttaste zeigen Hintergrund Farbe Rücktaste Modus Baked Bluetooth unten Break Hell Umschalt Mitte Farbe Compose Composed Komprimieren Sie Wiederholungen nach Strg+ entf Bildschirm Bildschirm Auswerfen Emacs ende Esc F1 F10 F11 F12 F2 F3 F4 F5 F6 F7 F8 F9 Fest Schrift Schrift Farbe Full ausblenden Pos1 Hyper+ einfg Tastatur Modus Taste Keysyms Groß Linux Mac Mittel Modifikatormodus Maus Mute Nächser Normal Num Deckkraft Pause Dauerhaftes Fenster Bild↓ Bild↑ Position Einstellungen voriger Drucken Beenden Raw Rec Neustart rollen Bildschirm Screencast deine Tasten  Screenkey konnte nicht initialisiert werden. Dies ist in der Regel ein Zeichen für eine unsachgemäßekonfigurierte Eingabemethode oder Desktop-Tastatureinstellungen. Bitte beachten Sie die <ahref = "{url}"> Dokumentation zur Fehlerbehebung </a> zur weiteren DiagnoseAnweisungen. \ n\ nScreenkey kann nicht wiederhergestellt werden und wird jetzt beendet! Suchen Auswahl Fenster/Region Umschalt+ Nur Modifikatorsequenzen anzeigen zeige Maus Leerzeichen anzeigen Zeige Tasten Größe schmal Super Super+ Text Farbe Zeit Oben Translated Vol WLAN Win Windows aus an Sekunden ← ↑ → ↓ ↹ ⇧+ ⌘+ ⌥+ ⌫ ⏎ ␣ ▶ ⬛  .                 🛠 🟊 